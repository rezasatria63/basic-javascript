console.log("Hai bro!");
window.alert("Hello, ini adalah program JS eksternal!");

var nama = prompt("Siapa nama kamu?", "");  //input var nama
document.write("<p>Hello "+ nama +"</p>");
document.write("<p>------------------</p>");

console.log("Aritmatika");
document.write("Aritmatika <br/>");
//aritmatika
var a = 15;
var b = 4;
var c = 0;

// pengurangan
c = a - b;
document.write(`${a} - ${b} = ${c}<br/>`);

// Perkalian
c = a * b;
document.write(`${a} * ${b} = ${c}<br/>`);

// pemangkatan
c = a ** b;
document.write(`${a} ** ${b} = ${c}<br/>`);

// Pembagian
c = a / b;
document.write(`${a} / ${b} = ${c}<br/>`);

// Modulo
c = a % b;
document.write(`${a} % ${b} = ${c}<br/>`);
document.write("<p>------------------</p>");

//Operator Penggabungan Teks
console.log("Operator Penggabungan Teks");
document.write("Operator Penggabungan Teks (Concat)<br/>");
var myconcat = "10" + "2";
document.write("\"10\" + \"2\" <br>");
document.write(myconcat + "<br>");
document.write("<p>------------------</p>");

console.log("Operator Penugasan");
document.write("Operator Penugasan<br>");
document.write("Mula-mula nilai score...<br>");
// pengisian nilai
var score = 100;
document.write("score = "+ score + "<br/>");

// pengisian dan menjumlahan dengan 5
score += 5;
document.write("--- score +=5 ---<br>");
document.write("score = "+ score + "<br><br/>");

// pengisian dan pengurangan dengan 2
score -= 2;
document.write("--- score -=2 ---<br>");
document.write("score = "+ score + "<br><br/>");

// pengisian dan perkalian dengan 2
score *= 2;
document.write("--- score *=2 ---<br>");
document.write("score = "+ score + "<br><br/>");

// pengisian dan pembagian dengan 4
score /= 4;
document.write("--- score /=4 ---<br>");
document.write("score = "+ score + "<br><br/>");

// pengisian dan pemangkatan dengan 2
score **= 2;
document.write("--- score **=2 ---<br>");
document.write("score = "+ score + "<br><br/>");

// pengisian dan modulo dengan 3;
score %= 3;
document.write("--- score %=3 ---<br>");
document.write("score = "+ score + "<br><br/>");
document.write("<p>------------------</p>");

//Operator Perbandingan
console.log("Operator Perbandingan");
document.write("Operator Perbandingan<br>");
var aku = 20;
var kamu = 19;

// sama dengan
var hasil = aku == kamu;
document.write(`${aku} == ${kamu} = ${hasil}<br/>`);

// lebih besar
var hasil = aku > kamu;
document.write(`${aku} > ${kamu} = ${hasil}<br/>`);

// lebih besar samadengan
var hasil = aku >= kamu;
document.write(`${aku} >= ${kamu} = ${hasil}<br/>`);

// lebih kecil
var hasil = aku < kamu;
document.write(`${aku} < ${kamu} = ${hasil}<br/>`);

// lebih kecil samadengan
var hasil = aku <= kamu;
document.write(`${aku} <= ${kamu} = ${hasil}<br/>`);

// tidak samadengan
var hasil = aku != kamu;
document.write(`${aku} != ${kamu} = ${hasil}<br/>`);
document.write("<p>------------------</p>");

console.log("Operator Logika");
document.write("Operator Logika<br/>");
var aku = 20;
var kamu = 19;

var benar = aku > kamu;
var salah = aku < kamu;

// operator && (and)
var hasil = benar && salah;
document.write(`${benar} && ${salah} = ${hasil}<br/>`);

// operator || (or)
var hasil = benar || salah;
document.write(`${benar} || ${salah} = ${hasil}<br/>`);

// operator ! (not)
var hasil = !benar
document.write(`!${benar} = ${hasil}<br/>`);
document.write("<p>------------------</p>");

